
import XCTest
@testable import Promise

class PromiseTests: XCTestCase {
    
    private var promiseHolderString: Promise<String>?
    private var promiseHolderStringArray: Promise<[String]>?
    private var promiseHolderInt: Promise<Int>?
    private let testQueue = DispatchQueue(label: "test queue")
    
    func testSuccessAndCompletionPromise() {
        let testString = "Success string"
        let expectationSuccess = self.expectation(description: "success")
        let expectationCompleted = self.expectation(description: "completed")
        promiseHolderString = ResolvedPromise<String>(queue: testQueue, result: testString)
            .onSuccess({ result in
                XCTAssertEqual(testString, result)
                expectationSuccess.fulfill()
            })
            .onCompleted({
                expectationCompleted.fulfill()
            })
        
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testErrorAndCompletionPromise() {
        let expectationFailure = self.expectation(description: "failure")
        let expectationCompleted = self.expectation(description: "completed")
        promiseHolderString = FailedPromise<String>(queue: testQueue, error: TestError.other)
            .onFailure({ error in
                XCTAssertNotNil(error as? TestError)
                XCTAssertEqual(error as! TestError, TestError.other)
                expectationFailure.fulfill()
            })
            .onCompleted({
                expectationCompleted.fulfill()
            })
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testCancelAndCompletion() {
        let expectationCancel = self.expectation(description: "cancel")
        let expectationCompleted = self.expectation(description: "completed")
        promiseHolderString = FailedPromise<String>(queue: testQueue, error: TestError.other)
            .onCancel({
                expectationCancel.fulfill()
            })
            .onCompleted({
                expectationCompleted.fulfill()
            })
        promiseHolderString!.cancel()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testProgressOnResolvedPromise() {
        let expectationProgressZero = self.expectation(description: "progress zero")
        let expectationProgressOne = self.expectation(description: "progress one")
        
        promiseHolderString = ResolvedPromise(queue: testQueue, result: "")
            .onProgressChanged({ progress in
                switch progress {
                case 0:
                    expectationProgressZero.fulfill()
                case 1:
                    expectationProgressOne.fulfill()
                default:
                    XCTAssert(false)
                }
                
            })
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testProgressOnSequencePromise() {
        let expectationProgressZero = self.expectation(description: "progress zero")
        let expectationProgressOneThird = self.expectation(description: "progress one third")
        let expectationProgressTwoThirds = self.expectation(description: "progress two thirds")
        let expectationProgressOne = self.expectation(description: "progress one")
        
        let firstPromise = ResolvedPromise(queue: testQueue, result: "")
        let secondPromise = ResolvedPromise(queue: testQueue, result: "")
        let thirdPromise = ResolvedPromise(queue: testQueue, result: "")
        
        promiseHolderStringArray = SequencePromise(queue: testQueue,
                                                   sequencePromises: [firstPromise, secondPromise, thirdPromise])
            .onProgressChanged({ (progress) in
                switch progress {
                case 0:
                    expectationProgressZero.fulfill()
                case 0.332...0.33334:
                    expectationProgressOneThird.fulfill()
                case 0.662...0.6667:
                    expectationProgressTwoThirds.fulfill()
                case 1:
                    expectationProgressOne.fulfill()
                default:
                    XCTAssert(false)
                }
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testMapProgress() {
        let expectationProgressZero = self.expectation(description: "progress zero")
        let expectationProgressOneThird = self.expectation(description: "progress one third")
        let expectationProgressOne = self.expectation(description: "progress one")
        
        let firstPromise = ResolvedPromise(queue: testQueue, result: "")
        let secondPromise = ResolvedPromise(queue: testQueue, result: "")
        
        promiseHolderStringArray = SequencePromise(queue: testQueue,
                                                   sequencePromises: [firstPromise, secondPromise])
            .mapProgress({ progress in
                switch progress {
                    
                case 0:
                    return 0
                    
                case 0.5:
                    return 0.333
                    
                case 1:
                    return 1
                    
                default:
                    XCTFail()
                    fatalError()
                    
                }
            })
            .onProgressChanged({ (progress) in
                switch progress {
                case 0:
                    expectationProgressZero.fulfill()
                case 0.333:
                    expectationProgressOneThird.fulfill()
                case 1:
                    expectationProgressOne.fulfill()
                default:
                    XCTFail()
                }
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testMapSuccess() {
        let initialString = "Initial string"
        let successValue = 2
        let expectationSuccess = self.expectation(description: "success")
        promiseHolderInt = ResolvedPromise(queue: self.testQueue, result: initialString)
            .mapSuccess({ result -> Int in
                if result == initialString {
                    return successValue
                }
                
                throw TestError.badValue
            })
            .onSuccess({ result in
                XCTAssertEqual(successValue, result)
                expectationSuccess.fulfill()
            })
        promiseHolderInt!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testMapErrorToSuccess() {
        let successString = "success string"
        let expectationSuccess = self.expectation(description: "success")
        promiseHolderString = FailedPromise<String>(queue: self.testQueue, error: TestError.other)
            .mapErrorToSuccess({ error in
                if error as? TestError == TestError.other {
                    return successString
                }
                
                throw TestError.badValue
            })
            .onSuccess({ result in
                XCTAssertEqual(successString, result)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testFlatMapSuccess() {
        let initialString = "Initial string"
        let successValue = 2
        let expectationSuccess = self.expectation(description: "success")
        promiseHolderInt = ResolvedPromise<String>(queue: self.testQueue, result: initialString)
            .flatMapSuccess({ result -> Promise<Int> in
                if result == initialString {
                    return ResolvedPromise<Int>(queue: self.testQueue, result: successValue)
                }
                
                throw TestError.badValue
            })
            .onSuccess({ result in
                XCTAssertEqual(successValue, result)
                expectationSuccess.fulfill()
            })
        promiseHolderInt!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testFlatMapErrorToSuccess() {
        let successString = "success string"
        let expectationSuccess = self.expectation(description: "success")
        promiseHolderString = FailedPromise<String>(queue: self.testQueue, error: TestError.badValue)
            .flatMapErrorToSuccess({ (error) -> Promise<String> in
                XCTAssertNotNil(error as? TestError)
                XCTAssertEqual(error as! TestError, TestError.badValue)
                
                return ResolvedPromise(queue: self.testQueue, result: successString)
            })
            .onSuccess({ result in
                XCTAssertEqual(successString, result)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testResolvedPromise() {
        let successResult = "success result"
        let expectationSuccess = self.expectation(description: "success")
        
        promiseHolderString = ResolvedPromise(queue: self.testQueue, result: successResult)
            .onSuccess({ result in
                XCTAssertEqual(result, successResult)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        waitForExpectations(timeout: 0.01, handler: nil)
    }
    
    func testDelayedPromiseSuccess() {
        let successResult = "Success result"
        let successPromise = ResolvedPromise(queue: self.testQueue, result: successResult)
        
        var result: String? = nil
        
        self.promiseHolderString = DelayedPromise(queue: self.testQueue,
                                                  delay: 0.2,
                                                  promise: successPromise)
            .onSuccess({ promiseResult in
                result = promiseResult
                XCTAssertEqual(result, successResult)
            })
        
        self.promiseHolderString!.resume()
        
        self.testQueue.asyncAfter(deadline: .now() + 0.1) {
            XCTAssertNil(result)
        }
        
        let expectationSuccess = self.expectation(description: "success")
        self.testQueue.asyncAfter(deadline: .now() + 0.3) {
            XCTAssertNotNil(result)
            XCTAssertEqual(result!, successResult)
            expectationSuccess.fulfill()
        }
        
        waitForExpectations(timeout: 0.4, handler: nil)
    }
    
    func testDelayedPromiseExtensionSuccess() {
        let successResult = "Success result"
        var result: String? = nil
        
        self.promiseHolderString = ResolvedPromise(queue: self.testQueue, result: successResult)
            .delay(by: 0.2)
            .onSuccess({ promiseResult in
                result = promiseResult
                XCTAssertEqual(result, successResult)
            })
        
        self.promiseHolderString!.resume()
        
        self.testQueue.asyncAfter(deadline: .now() + 0.1) {
            XCTAssertNil(result)
        }
        
        let expectationSuccess = self.expectation(description: "success")
        self.testQueue.asyncAfter(deadline: .now() + 0.3) {
            XCTAssertNotNil(result)
            XCTAssertEqual(result!, successResult)
            expectationSuccess.fulfill()
        }
        
        waitForExpectations(timeout: 0.4, handler: nil)
    }
    
    func testRivalPromiseEmptyFailure() {
        let expectationFailure = self.expectation(description: "failure")
        
        promiseHolderString = RivalPromise(queue: self.testQueue, competingPromises: [])
            .onFailure({ error in
                XCTAssert(error as? RivalPromiseError == .noCompetingPromises)
                expectationFailure.fulfill()
            })
        promiseHolderString!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testRivalPromiseBothSuccess() {
        let firstParameter = "First"
        let secondParameter = "Second"
        let expectationSuccess = self.expectation(description: "success")
        
        let competingPromise1 = ResolvedPromise(queue: self.testQueue, result: firstParameter)
            .delay(by: 0.1)
        let competingPromise2 = ResolvedPromise(queue: self.testQueue, result: secondParameter)
        
        promiseHolderString = RivalPromise(queue: self.testQueue, competingPromises: [competingPromise1, competingPromise2])
            .onSuccess({ result in
                XCTAssertEqual(result, secondParameter)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testRivalPromiseFirstFailureSecondSuccess() {
        let resultParameter = "Second"
        let expectationSuccess = self.expectation(description: "success")
        
        let competingPromise1 = FailedPromise<String>(queue: self.testQueue, error: TestError.other)
        let competingPromise2 = ResolvedPromise(queue: self.testQueue, result: resultParameter).delay(by: 0.1)
        
        promiseHolderString = RivalPromise(queue: self.testQueue, competingPromises: [competingPromise1, competingPromise2])
            .onSuccess({ result in
                XCTAssertEqual(result, resultParameter)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testRivalPromiseBothFailures() {
        let firstError = TestError.badValue
        let secondError = TestError.other
        
        let expectationSuccess = self.expectation(description: "success")
        
        let competingPromise1 = FailedPromise<String>(queue: self.testQueue, error: firstError)
        let competingPromise2 = FailedPromise<String>(queue: self.testQueue, error: secondError).delay(by: 0.1)
        
        promiseHolderString = RivalPromise(queue: self.testQueue, competingPromises: [competingPromise1, competingPromise2])
            .onFailure({ error in
                XCTAssert(error as? TestError == secondError)
                expectationSuccess.fulfill()
            })
        promiseHolderString!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testBranchedCancel() {
        let successResult = "Success result"
        let expectationCanceled = self.expectation(description: "canceled")
        let expectationSuccess = self.expectation(description: "success")
        let basePromise = ResolvedPromise(queue: self.testQueue, result: successResult)
        let branchToCancel = basePromise.onCancel({
            expectationCanceled.fulfill()
        })
        let branchToGetResult = basePromise.onSuccess({ result in
            XCTAssertEqual(successResult, result)
            expectationSuccess.fulfill()
        })
        promiseHolderString = branchToGetResult
        
        branchToCancel.cancel()
        promiseHolderString!.resume()
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testExternalPromise() {
        let successResult = "Success result"
        let resumeExpectation = self.expectation(description: "resumed")
        let resultExpectation = self.expectation(description: "success")
        
        let externalPromise = ExternalPromise<String>(queue: self.testQueue, onResume: {
            resumeExpectation.fulfill()
        })
            
        promiseHolderString = externalPromise.onSuccess({
            resultExpectation.fulfill()
            XCTAssertEqual(successResult, $0)
        })
        promiseHolderString!.resume()
        
        externalPromise.complete(with: successResult)
        
        waitForExpectations(timeout: 0.1, handler: nil)
    }
    
    func testSequenceEmpty() {
        let successExpectation = self.expectation(description: "Success")
        
        promiseHolderStringArray = SequencePromise(queue: testQueue, sequencePromises: [])
            .onSuccess({ result in
                XCTAssertEqual(result, [])
                successExpectation.fulfill()
            })

        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testSequenceOnePromiseSuccess() {
        let successExpectation = self.expectation(description: "Success")
        let successResult1 = "Success result"
        let firstPromise = ResolvedPromise(queue: testQueue, result: successResult1)
        
        promiseHolderStringArray = SequencePromise(queue: testQueue, sequencePromises: [firstPromise])
            .onSuccess({ result in
                XCTAssertEqual([successResult1], result)
                successExpectation.fulfill()
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testSequenceMultiplePromiseSuccess() {
        let successExpectation = self.expectation(description: "Success")
        let successResult1 = "Success result"
        let successResult2 = "Success result 2"
        let successResult3 = "Success result 3"
        
        let firstPromise = ResolvedPromise(queue: testQueue, result: successResult1)
        let secondPromise = ResolvedPromise(queue: testQueue, result: successResult2)
        let thirdPromise = ResolvedPromise(queue: testQueue, result: successResult3)
        
        promiseHolderStringArray = SequencePromise(queue: testQueue, sequencePromises: [firstPromise, secondPromise, thirdPromise])
            .onSuccess({ result in
                XCTAssertEqual([successResult1, successResult2, successResult3], result)
                successExpectation.fulfill()
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testSequenceFirstPromiseFailure() {
        let failureExpectation = self.expectation(description: "Error")
        
        let firstPromise = FailedPromise<String>(queue: DispatchQueue.main, error: TestError.other)
        let secondPromise = ResolvedPromise(queue: testQueue, result: "Any string")
        
        promiseHolderStringArray = SequencePromise(queue: testQueue, sequencePromises: [firstPromise, secondPromise])
            .onFailure({ _ in
                failureExpectation.fulfill()
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
    func testSequenceLastPromiseFailure() {
        let failureExpectation = self.expectation(description: "Error")
        
        let firstPromise = ResolvedPromise(queue: testQueue, result: "Any string")
        let secondPromise = FailedPromise<String>(queue: DispatchQueue.main, error: TestError.other)
        
        promiseHolderStringArray = SequencePromise(queue: testQueue, sequencePromises: [firstPromise, secondPromise])
            .onFailure({ _ in
                failureExpectation.fulfill()
            })
        promiseHolderStringArray!.resume()
        
        waitForExpectations(timeout: 0.2, handler: nil)
    }
    
}

enum TestError: Error {
    case other
    case badValue
}
