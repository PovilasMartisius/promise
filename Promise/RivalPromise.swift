import Foundation

public enum RivalPromiseError: Error {
    
    case noCompetingPromises
    
}

public class RivalPromise<T>: PromiseSubject<T> {
    
    private var competingPromises: [Promise<T>]
    
    public init(queue: DispatchQueue, competingPromises: [Promise<T>]) {
        self.competingPromises = competingPromises
        
        super.init(queue: queue)
    }
    
    public override func start() {
        guard self.competingPromises.count > 0 else {
            complete(with: RivalPromiseError.noCompetingPromises)
            return
        }
        
        self.competingPromises = self.competingPromises.map({ promise in
            promise
                .callback(on: self.callbackQueue)
                .onSuccess({ [weak self] result  in
                    self?.customComplete(with: result)
                })
                .onFailure({ [weak self] error in
                    self?.customComplete(with: error)
                })
        })
        
        self.competingPromises.forEach({
            $0.resume()
        })
    }
    
    public override func stop() {
        self.competingPromises.forEach({ $0.cancel() })
        self.competingPromises.removeAll()
    }
    
    private var completedPromisesCount: Int = 0
    private func customComplete(with error: Error) {
        completedPromisesCount += 1
        
        guard competitionEnded == false else {
            return
        }
        
        guard completedPromisesCount == self.competingPromises.count else {
            return
        }
        
        self.competingPromises.forEach({ $0.cancel() })
        self.competitionEnded = true
        self.finish(with: error)
    }
    
    private var competitionEnded: Bool = false
    private func customComplete(with result: T) {
        completedPromisesCount += 1
        competitionEnded = true
        
        self.competingPromises.forEach({ $0.cancel() })
        self.finish(with: result)
    }
    
}
