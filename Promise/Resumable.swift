
import Foundation

public protocol Resumable: Cancelable {
    
    func resume()
    
}
