import Foundation

public protocol Cancelable: class {
    
    func cancel()
    
}
