
import Foundation

public class SequencePromise<T>: PromiseSubject<[T]> {
    
    private var sequencePromises: [Promise<T>]
    
    public init(queue: DispatchQueue, sequencePromises: [Promise<T>]) {
        self.sequencePromises = sequencePromises
        
        super.init(queue: queue)
    }
    
    private var currentPromiseIndex: Int = 0
    
    public override func start() {
        guard self.sequencePromises.count > 0 else {
            complete(with: [])
            return
        }
        
        self.sequencePromises = self.sequencePromises.map({ promise in
            promise
                .callback(on: self.callbackQueue)
                .onProgressChanged({ [weak self] (progress) in
                    self?.customUpdate(with: progress)
                })
                .onSuccess({ [weak self] result  in
                    self?.customComplete(with: result)
                })
                .onFailure({ [weak self] error in
                    self?.complete(with: error)
                })
        })
        
        self.sequencePromises.first!.resume()
    }
    
    public override func stop() {
        self.sequencePromises.forEach({ $0.cancel() })
        self.sequencePromises.removeAll()
    }
    
    private var results: [T] = []
    private func customComplete(with result: T) {
        results.append(result)
        
        currentPromiseIndex += 1
        
        guard currentPromiseIndex != self.sequencePromises.count else {
            self.finish(with: results)
            return
        }
        
        self.sequencePromises[currentPromiseIndex].resume()
    }
    
    private func customUpdate(with progress: Float) {
        let totalPromises = Float(self.sequencePromises.count)
        let currentPromise = Float(self.currentPromiseIndex)
        let fullyCompletedPromisesProgress = currentPromise / totalPromises
        let partialCompletionProgress = progress / totalPromises
        let totalProgress = fullyCompletedPromisesProgress + partialCompletionProgress
        self.update(with: totalProgress)
    }
    
}
