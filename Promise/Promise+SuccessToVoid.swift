
import Foundation

public extension Promise {

    public func mapSuccessToVoid() -> Promise<Void> {
        return self.mapSuccess({ _ in
            return ()
        })
    }
    
}
