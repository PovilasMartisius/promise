
import Foundation

/**
    Subclass this class to create promise subject.
    Default implementation does nothing.
 
    Functions to implement:
    * start: this function is called when promise is resumed.
             Use it to start execution.
             'finish:with' must be called at the end of execution.
    * stop: this function is called when promise is canceled.
            Use it to break current execution if possible.
            'finish:with' should NOT be called.
 */
open class PromiseSubject<T: Any> : PromiseImplementation<T> {
    
    public init(queue: DispatchQueue) {
        super.init(parent: nil, queue: queue)
    }
    
    private override init(parent: PromiseParentProtocol?, queue: DispatchQueue) {
        super.init(parent: parent, queue: queue)
    }
    
    private override init(parent: PromiseParentProtocol?, queue: DispatchQueue, successBlock: @escaping SuccessBlock) {
        super.init(parent: parent, queue: queue, successBlock: successBlock)
    }
    
    private override init(parent: PromiseParentProtocol?, queue: DispatchQueue, failureBlock: @escaping FailureBlock) {
        super.init(parent: parent, queue: queue, failureBlock: failureBlock)
    }
    
    private override init(parent: PromiseParentProtocol?, queue: DispatchQueue, cancelBlock: @escaping CancelBlock) {
        super.init(parent: parent, queue: queue, cancelBlock: cancelBlock)
    }
    
    private var started = false
    public final override func resume() {
        callbackQueue.async {
            var started = false
            
            self.syncQueue.sync {
                started = self.started
                self.started = true
            }
            
            guard started == false else {
                return
            }
            
            self.update(with: 0)
            self.start()
        }
    }
    
    internal var stopped = false
    public final override func cancel() {
        callbackQueue.async {
            var stopped = false
            
            self.syncQueue.sync {
                stopped = self.stopped
                self.stopped = true
            }
            
            guard stopped == false else {
                return
            }
            
            self.stop()
        }
        
        super.cancel()
    }
    
    open func start() {
        
    }
    
    open func stop() {
        
    }
    
    public final override func update(with progress: Float) {
        super.update(with: progress)
    }
    
    public final func finish(with result: T) {
        self.syncQueue.sync {
            self.stopped = true
        }
        
        callbackQueue.async {
            self.complete(with: result)
        }
    }
    
    public final func finish(with error: Error) {
        self.syncQueue.sync {
            self.stopped = true
        }
        
        callbackQueue.async {
            self.complete(with: error)
        }
    }
    
}
