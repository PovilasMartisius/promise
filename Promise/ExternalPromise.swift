
import Foundation

public class ExternalPromise<T>: PromiseSubject<T> {
    
    private let onResume: (Void) -> Void
    
    public init(queue: DispatchQueue, onResume: @escaping (Void) -> Void) {
        self.onResume = onResume
        
        super.init(queue: queue)
    }
    
    override public func start() {
        onResume()
    }
    
}
