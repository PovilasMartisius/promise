
import Foundation

private struct WeakPromiseReferenceHolder<T: Any> {
    
    weak var object: PromiseImplementation<T>?
    
    init(object: PromiseImplementation<T>) {
        self.object = object
    }
    
}

open class PromiseImplementation<T: Any>: Promise<T> {
    
    private var children: [WeakPromiseReferenceHolder<T>] = []
    let callbackQueue: DispatchQueue
    let syncQueue = DispatchQueue(label: "Promise Sync Queue")
    private var parent: PromiseParentProtocol?
    
    private var successBlock: SuccessBlock?
    private var failureBlock: FailureBlock?
    private var cancelBlock: CancelBlock?
    private var completedBlock: CompletedBlock?
    private var progressBlock: ProgressBlock?
    
    private var result: T?
    private var error: Error?
    private var canceled: Bool = false
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue) {
        self.parent = parent
        self.callbackQueue = queue
        
        super.init()
    }
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue, successBlock: @escaping SuccessBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.successBlock = successBlock
        
        super.init()
    }
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue, progressBlock: @escaping ProgressBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.progressBlock = progressBlock
        
        super.init()
    }
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue, failureBlock: @escaping FailureBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.failureBlock = failureBlock
        
        super.init()
    }
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue, cancelBlock: @escaping CancelBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.cancelBlock = cancelBlock
        
        super.init()
    }
    
    init(parent: PromiseParentProtocol?, queue: DispatchQueue, completedBlock: @escaping CompletedBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.completedBlock = completedBlock
        
        super.init()
    }
    
    private init(parent: PromiseParentProtocol?,
                 queue: DispatchQueue,
                 progressBlock: @escaping ProgressBlock,
                 successBlock: @escaping SuccessBlock,
                 failureBlock: @escaping FailureBlock,
                 cancelBlock: @escaping CancelBlock) {
        self.parent = parent
        self.callbackQueue = queue
        self.progressBlock = progressBlock
        self.successBlock = successBlock
        self.failureBlock = failureBlock
        self.cancelBlock = cancelBlock
        
        super.init()
    }
    
    public final override func callback(on queue: DispatchQueue) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: queue)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public final override func onSuccess(_ successBlock: @escaping SuccessBlock) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: callbackQueue, successBlock: successBlock)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public override func onProgressChanged(_ progressBlock: @escaping ProgressBlock) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: callbackQueue, progressBlock: progressBlock)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public final override func onFailure(_ failureBlock: @escaping FailureBlock) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: callbackQueue, failureBlock: failureBlock)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public final override func onCancel(_ cancelBlock: @escaping CancelBlock) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: callbackQueue, cancelBlock: cancelBlock)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public final override func onCompleted(_ completedBlock: @escaping CompletedBlock) -> Promise<T> {
        var child: PromiseImplementation!
        
        self.syncQueue.sync {
            child = PromiseImplementation(parent: self, queue: callbackQueue, completedBlock: completedBlock)
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: child)
            children.append(weakReferenceHolder)
        }
        
        return child
    }
    
    public final override func mapProgress(_ mapper: @escaping (Float) -> Float) -> Promise<T>  {
        let convertedPromise: PromiseImplementation<T>! = PromiseImplementation<T>(parent: nil, queue: callbackQueue)
        
        syncQueue.sync {
            let convertedPromiseParent = PromiseImplementation(parent: self,
                                                               queue: self.callbackQueue,
                                                               progressBlock: { [weak convertedPromise] progress in
                                                                    convertedPromise?.update(with: mapper(progress))
                                                               },
                                                               successBlock: { [weak convertedPromise] result in
                                                                    convertedPromise?.complete(with: result)
                                                               },
                                                               failureBlock: { [weak convertedPromise] error in
                                                                    convertedPromise?.complete(with: error)
                                                               },
                                                               cancelBlock: { [weak convertedPromise] in
                                                                    convertedPromise?.cancel()
                                                               })
            
            convertedPromise.parent = convertedPromiseParent
            
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: convertedPromiseParent)
            self.children.append(weakReferenceHolder)
        }
        
        return convertedPromise
    }
    
    public final override func mapSuccess<G: Any>(_ mapper: @escaping (T) throws -> G) -> Promise<G>  {
        let convertedPromise: PromiseImplementation<G>! = PromiseImplementation<G>(parent: nil, queue: callbackQueue)
        
        syncQueue.sync {
            let convertedPromiseParent = PromiseImplementation(parent: self,
                                                               queue: self.callbackQueue,
                                                               progressBlock: { [weak convertedPromise] progress in
                                                                    convertedPromise?.update(with: progress)
                                                               },
                                                               successBlock: { [weak convertedPromise] result in
                                                                    do {
                                                                        let result = try mapper(result)
                                                                        convertedPromise?.complete(with: result)
                                                                    } catch {
                                                                        convertedPromise?.complete(with: error)
                                                                    }
                                                               },
                                                               failureBlock: { [weak convertedPromise] error in
                                                                    convertedPromise?.complete(with: error)
                                                               },
                                                               cancelBlock: { [weak convertedPromise] in
                                                                    convertedPromise?.cancel()
                                                               })
            
            convertedPromise.parent = convertedPromiseParent
            
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: convertedPromiseParent)
            self.children.append(weakReferenceHolder)
        }
        
        return convertedPromise
    }
    
    public final override func flatMapSuccess<G: Any>(_ mapper: @escaping (T) throws -> Promise<G>) -> Promise<G> {
        let convertedPromise: PromiseImplementation<G>! = PromiseImplementation<G>(parent: nil, queue: callbackQueue)
        
        syncQueue.sync {
            let convertedPromiseParent = PromiseImplementation(parent: self,
                                                               queue: self.callbackQueue,
                                                               progressBlock: { [weak convertedPromise] progress in
                                                                    convertedPromise?.update(with: progress)
                                                               },
                                                               successBlock: { [weak convertedPromise] result in
                                                                    do {
                                                                        var promise: Promise<G>! = try mapper(result)
                                                                            .onProgressChanged({ [weak convertedPromise] progress in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.update(with: progress)
                                                                                }
                                                                            })
                                                                            .onSuccess({ [weak convertedPromise] result in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.complete(with: result)
                                                                                }
                                                                            }).onFailure({ [weak convertedPromise] error in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.complete(with: error)
                                                                                }
                                                                            }).onCancel({ [weak convertedPromise] in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.cancel()
                                                                                }
                                                                            })
                                                                        
                                                                        promise = promise.onCompleted({
                                                                            promise = nil
                                                                        })
                                                                        promise.resume()
                                                                    } catch {
                                                                        convertedPromise?.complete(with: error)
                                                                    }
                                                               },
                                                               failureBlock: { [weak convertedPromise] error in
                                                                    convertedPromise?.complete(with: error)
                                                               },
                                                               cancelBlock: { [weak convertedPromise] in
                                                                    convertedPromise?.cancel()
                                                               })
            
            convertedPromise.parent = convertedPromiseParent
            
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: convertedPromiseParent)
            self.children.append(weakReferenceHolder)
        }
        
        return convertedPromise
    }
    
    public final override func flatMapErrorToSuccess(_ mapper: @escaping (Error) throws -> Promise<T>) -> Promise<T> {
        let convertedPromise: PromiseImplementation<T>! = PromiseImplementation<T>(parent: nil, queue: callbackQueue)
        
        syncQueue.sync {
            let convertedPromiseParent = PromiseImplementation(parent: self,
                                                               queue: self.callbackQueue,
                                                               progressBlock: { [weak convertedPromise] progress in
                                                                    convertedPromise?.update(with: progress)
                                                               },
                                                               successBlock: { [weak convertedPromise] result in
                                                                    convertedPromise?.complete(with: result)
                                                               },
                                                               failureBlock: { [weak convertedPromise] error in
                                                                    do {
                                                                        var promise: Promise<T>! = try mapper(error)
                                                                            .onProgressChanged({ [weak convertedPromise] progress in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.update(with: progress)
                                                                                }
                                                                            })
                                                                            .onSuccess({ [weak convertedPromise] result in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.complete(with: result)
                                                                                }
                                                                            }).onFailure({ [weak convertedPromise] error in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.complete(with: error)
                                                                                }
                                                                            }).onCancel({ [weak convertedPromise] in
                                                                                self.callbackQueue.async {
                                                                                    convertedPromise?.cancel()
                                                                                }
                                                                            })
                                                                        
                                                                        promise = promise.onCompleted({
                                                                            promise = nil
                                                                        })
                                                                        promise.resume()
                                                                    } catch {
                                                                        convertedPromise?.complete(with: error)
                                                                    }
                                                               },
                                                               cancelBlock: { [weak convertedPromise] in
                                                                    convertedPromise?.cancel()
                                                               })
            
            convertedPromise.parent = convertedPromiseParent
            
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: convertedPromiseParent)
            self.children.append(weakReferenceHolder)
        }
        
        return convertedPromise
    }
    
    public override func mapErrorToSuccess(_ mapper: @escaping (Error) throws -> T) -> Promise<T>  {
        let convertedPromise: PromiseImplementation<T>! = PromiseImplementation<T>(parent: nil, queue: callbackQueue)
        
        syncQueue.sync {
            let convertedPromiseParent = PromiseImplementation(parent: self,
                                                               queue: self.callbackQueue,
                                                               progressBlock: { [weak convertedPromise] progress in
                                                                    convertedPromise?.update(with: progress)
                                                               },
                                                               successBlock: { [weak convertedPromise] result in
                                                                    convertedPromise?.complete(with: result)
                                                               },
                                                               failureBlock: { [weak convertedPromise] error in
                                                                    do {
                                                                        let result = try mapper(error)
                                                                        convertedPromise?.complete(with: result)
                                                                    } catch {
                                                                        convertedPromise?.complete(with: error)
                                                                    }
                                                               },
                                                               cancelBlock: { [weak convertedPromise] in
                                                                    convertedPromise?.cancel()
                                                               })
            
            convertedPromise.parent = convertedPromiseParent
            
            let weakReferenceHolder = WeakPromiseReferenceHolder(object: convertedPromiseParent)
            self.children.append(weakReferenceHolder)
        }
        
        return convertedPromise
    }
    
    public override func resume() {
        callbackQueue.async {
            var parent: PromiseParentProtocol?
            var result: T?
            var error: Error?
            var canceled: Bool = false
            
            self.syncQueue.sync {
                parent = self.parent
                result = self.result
                error = self.error
                canceled = self.canceled
            }
            
            if parent != nil {
                parent!.resume()
                return
            }
            
            guard result == nil else {
                self.complete(with: result!)
                return
            }
            
            guard error == nil else {
                self.complete(with: error!)
                return
            }
            
            guard canceled == false else {
                self.cancel()
                return
            }
        }
    }
    
    public override func cancel() {
        callbackQueue.async {
            var parent: PromiseParentProtocol?
            var children: [WeakPromiseReferenceHolder<T>] = []
            var cancelBlock: CancelBlock?
            var completedBlock: CompletedBlock?
            var siblingsCount: Int!
            
            self.syncQueue.sync {
                if self.result == nil && self.error == nil {
                    self.canceled = true
                }
                
                parent = self.parent
                self.parent = nil
                
                siblingsCount = parent?.childrenCount() ?? 0
                
                children = self.children
                
                cancelBlock = self.cancelBlock
                completedBlock = self.completedBlock
                
                self.unsafeClearCallbacks()
            }
            
            cancelBlock?()
            completedBlock?()
            
            children.forEach({ $0.object?.cancel() })
            
            if siblingsCount <= 1 {
                parent?.cancel()
            }
        }
    }
    
    override func childrenCount() -> Int {
        return self.children.count
    }
    
    final func complete(with result: T) {
        callbackQueue.async {
            var progressBlock: ProgressBlock?
            var successBlock: SuccessBlock?
            var completedBlock: CompletedBlock?
            var children: [WeakPromiseReferenceHolder<T>] = []
            var notifyProgress: Bool = false
            
            self.syncQueue.sync {
                self.result = result
                notifyProgress = (self.currentProgress ?? 0) < Float(1)
                self.currentProgress = 1
                successBlock = self.successBlock
                progressBlock = self.progressBlock
                completedBlock = self.completedBlock
                self.unsafeClearCallbacks()
                
                children = self.children
                self.parent = nil
            }
            
            if notifyProgress {
                progressBlock?(1)
            }
            
            successBlock?(result)
            completedBlock?()
            
            children.forEach({ $0.object?.complete(with: result) })
        }
    }
    
    final func complete(with error: Error) {
        callbackQueue.async {
            var failureBlock: FailureBlock?
            var completedBlock: CompletedBlock?
            var children: [WeakPromiseReferenceHolder<T>] = []
            
            self.syncQueue.sync {
                self.error = error
                
                failureBlock = self.failureBlock
                completedBlock = self.completedBlock
                self.unsafeClearCallbacks()
                
                children = self.children
                
                self.parent = nil
            }
            
            failureBlock?(error)
            completedBlock?()
            
            children.forEach({ object in
                object.object?.complete(with: error)
            })
        }
    }
    
    private var currentProgress: Float?
    func update(with progress: Float) {
        callbackQueue.async {
            var progressBlock: ProgressBlock?
            var children: [WeakPromiseReferenceHolder<T>] = []
            var notifyProgress = false
            self.syncQueue.sync {
                notifyProgress = self.currentProgress == nil || progress > (self.currentProgress ?? 0)
                self.currentProgress = progress
                
                progressBlock = self.progressBlock
                children = self.children
            }
            
            guard notifyProgress else {
                return
            }
            
            progressBlock?(progress)
            
            children.forEach({ object in
                object.object?.update(with: progress)
            })
        }
    }
    
    private func unsafeClearCallbacks() {
        self.successBlock = nil
        self.failureBlock = nil
        self.cancelBlock = nil
        self.progressBlock = nil
        self.completedBlock = nil
    }
    
}
