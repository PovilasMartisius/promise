
import Foundation

public class FailedPromise<T>: PromiseSubject<T> {
    
    private let error: Error
    
    public init(queue: DispatchQueue, error: Error) {
        self.error = error
        
        super.init(queue: queue)
    }
    
    public override func start() {
        self.finish(with: error)
    }
    
}
