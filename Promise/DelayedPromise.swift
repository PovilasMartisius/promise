
import Foundation

public class DelayedPromise<T>: PromiseSubject<T> {
    
    private let delay: TimeInterval
    private var promise: Promise<T>
    
    public init(queue: DispatchQueue, delay: TimeInterval, promise: Promise<T>) {
        self.delay = delay
        self.promise = promise
        
        super.init(queue: queue)
    }
    
    public override func start() {
        self.syncQueue.asyncAfter(deadline: .now() + delay) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            guard strongSelf.stopped == false else {
                return
            }
            
            strongSelf.promise = strongSelf.promise.onSuccess({ [weak self] in
                self?.complete(with: $0)
            }).onFailure({ [weak self] in
                self?.complete(with: $0)
            })
            
            strongSelf.promise.resume()
        }
    }
    
    public override func stop() {
        self.promise.cancel()
    }
    
}

public extension Promise {

    public func delay(by: TimeInterval) -> Promise {
        return DelayedPromise(queue: (self as! PromiseImplementation).callbackQueue,
                              delay: by,
                              promise: self)
    }
    
}
