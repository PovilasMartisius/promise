
import Foundation

public class ResolvedPromise<T>: PromiseSubject<T> {
    
    private let result: T
    
    public init(queue: DispatchQueue, result: T) {
        self.result = result
        
        super.init(queue: queue)
    }
    
    override public func start() {
        finish(with: result)
    }
    
}
