
import Foundation

protocol ChildrenCountable {
    
    func childrenCount() -> Int
    
}

protocol PromiseParentProtocol: Resumable, ChildrenCountable {}

open class Promise <T: Any> : Resumable, PromiseParentProtocol {
    
    public typealias SuccessBlock = (T) -> Void
    public typealias FailureBlock = (Error) -> Void
    public typealias ProgressBlock = (Float) -> Void
    public typealias CancelBlock = (Void) -> Void
    public typealias CompletedBlock = CancelBlock

    public func callback(on queue: DispatchQueue) -> Promise<T> {
        fatalError()
    }
    
    public func onSuccess(_ successBlock: @escaping SuccessBlock) -> Promise<T> {
        fatalError()
    }
    
    public func onProgressChanged(_ progressBlock: @escaping ProgressBlock) -> Promise<T> {
        fatalError()
    }
    
    public func onFailure(_ failureBlock: @escaping FailureBlock) -> Promise<T> {
        fatalError()
    }
    
    public func onCancel(_ cancelBlock: @escaping CancelBlock) -> Promise<T> {
        fatalError()
    }
    
    public func onCompleted(_ completedBlock: @escaping CompletedBlock) -> Promise<T> {
        fatalError()
    }
    
    public func mapProgress(_ mapper: @escaping (Float) -> Float) -> Promise<T> {
        fatalError()
    }
    
    public func mapSuccess<G: Any>(_ mapper: @escaping (T) throws -> G ) -> Promise<G> {
        fatalError()
    }
    
    public func flatMapSuccess<G: Any>(_ mapper: @escaping (T) throws -> Promise<G>) -> Promise<G> {
        fatalError()
    }
    
    public func flatMapErrorToSuccess(_ mapper: @escaping (Error) throws -> Promise<T>) -> Promise<T> {
        fatalError()
    }
    
    public func mapErrorToSuccess(_ mapper: @escaping (Error) throws -> T) -> Promise<T> {
        fatalError()
    }
    
    public func cancel() {
        fatalError()
    }
    
    public func resume() {
        fatalError()
    }
    
    func childrenCount() -> Int {
        fatalError()
    }
    
}
